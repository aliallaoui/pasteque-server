<?php
//    Pasteque API
//
//    Copyright (C) 2012 Scil (http://scil.coop)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque;

/** Static class for easy logging. */
class Log {

    /** Debug level for technical info. */
    const LEVEL_DEBUG = 0;
    /** Non impacting event message level. */
    const LEVEL_INFO = 1;
    /** Impacting but non-breaking event message level. */
    const LEVEL_WARNING = 2;
    /** Breaking event message level. */
    const LEVEL_ERROR = 3;
    /** Totally unexpected and catastrophic event message level. */
    const LEVEL_WTF = 4;
    const LEVEL_DISABLE = 999;

    private static $loggers = array();
    private static $minLevel = Log::LEVEL_ERROR;

    private function __construct() {}

    public static function setStrLevel($level) {
        switch (strtoupper($level)) {
        case 'DEBUG': Log::setLevel(Log::LEVEL_DEBUG); return true;
        case 'INFO': Log::setLevel(Log::LEVEL_INFO); return true;
        case 'WARN':
        case 'WARNING': Log::setLevel(Log::LEVEL_WARNING); return true;
        case 'ERR':
        case 'ERROR': Log::setLevel(Log::LEVEL_ERROR); return true;
        case 'WTF': Log::setLevel(Log::LEVEL_WTF); return true;
        default: return false;
        }
    }
    public static function strLevel($level) {
        switch ($level) {
        case Log::LEVEL_DEBUG: return 'DEBUG';
        case Log::LEVEL_INFO: return 'INFO';
        case Log::LEVEL_WARNING: return 'WARN';
        case Log::LEVEL_ERROR: return 'ERROR';
        case Log::LEVEL_WTF: return '!!WTF!!';
    }
    return "";
    }

    public static function setLevel($level) {
        Log::$minLevel = $level;
    }

    public static function addLogger($logger) {
        Log::$loggers[] = $logger;
    }

    private static function log($level, $message) {
        if (Log::$minLevel <= $level) {
            // Prefix message by user id if any
            // Disable log to prevent infinite loop
            $semaphore = sem_get(3598);
            sem_acquire($semaphore);
            $currLevel = Log::$minLevel;
            Log::setLevel(Log::LEVEL_DISABLE);
            $userId = Login::getLoggedUser();
            Log::setLevel($currLevel);
            sem_release($semaphore);
            $strId = "Anon";
            if ($userId != null) {
                $strId = $userId;
            }
            // Prefix with time
            $time = date('ymd-H:i:s');
            // Set level
            $strLvl = Log::strLevel($level);
            foreach (Log::$loggers as $log) {
                $log->log($level, sprintf("[%s-%s-%s]%s\n",
                                $time, $strId, $strLvl, $message));
            }
        }
    }

    public static function debug($message) {
        Log::log(Log::LEVEL_DEBUG, $message);
    }
    public static function info($message) {
        Log::log(Log::LEVEL_INFO, $message);
    }
    public static function warn($message) {
        Log::log(Log::LEVEL_WARNING, $message);
    }
    public static function error($message) {
        Log::log(Log::LEVEL_ERROR, $message);
    }
    public static function wtf($message) {
        Log::log(Log::LEVEL_WTF, $message);
    }
}

/** Log messages to a given target. */
class Logger {

    /** Create a logger to log at given target.
     * @param $target The log directory path, system log if null. */
    public function __construct($target) {
        $this->target = $target;
        if (!empty($this->target) && !is_writeable($this->target)) {
            error_log($target . ' is not writeable! Log is disabled');
        }
    }
    public function log($level, $message) {
        if ($this->target === null) {
            error_log($message, 0);
        } else {
            $date = date('Ymd');
            $path = $this->target . '/' . $date . '.log';
            error_log($message, 3, $path);
        }
    }
}